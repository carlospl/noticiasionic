import { Component, Input, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Article } from 'src/app/interface/interface';
import { Plugins } from '@capacitor/core';
import { DataService } from 'src/app/services/data.service';
import { DataLocalService } from 'src/app/services/data-local.service';


@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss'],
})
export class ArticlesComponent implements OnInit {

  @Input() news: Article[]=[];
  constructor(public actionSheetController: ActionSheetController,public Data:DataService,public DataFav:DataLocalService) { 
  }

  ngOnInit() {}
  async lanzarMenu(i) {
    const actionSheet = await this.actionSheetController.create({
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Compartir',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
          this.comaprtir(i);
        }
      },{
        text: 'Favorite',
        icon: 'star',
        handler: () => {
          console.log("Guardado");
          this.DataFav.guardarNoticia(this.news[i]);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
  async openBrowser(i) {
    const { Browser } = Plugins;
    // On iOS, for example, this will open the URL in Safari instead of
    // the SFSafariViewController (in-app browser)
    await Browser.open({ url: this.news[i].url });
  }
  async comaprtir(i){
    const { Share } = Plugins;
    let shareRet = await Share.share({
      title: 'See cool stuff',
      text: 'Really awesome thing you need to see right meow',
      url: this.news[i].url,
      dialogTitle: 'Share with buddies'
    });
  }

}
