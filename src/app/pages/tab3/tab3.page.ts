import { Component } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { DataLocalService } from 'src/app/services/data-local.service';
import { Plugins } from '@capacitor/core';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  constructor(public dataFav:DataLocalService,public actionSheetController: ActionSheetController) {}
  ngOnInit(): void {
    this.dataFav.cargarFavoritos();
  }

  async lanzarMenu(i) {
    const actionSheet = await this.actionSheetController.create({
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Compartir',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      },{
        text: 'Quitar de Favoritos',
        icon: 'trash',
        handler: () => {
          console.log("Borrado");
          this.dataFav.noticias=this.dataFav.noticias.filter(noti =>noti.title !==this.dataFav.noticias[i].title )
          this.dataFav.storage.set('favoritos',this.dataFav.noticias);
          //this.DataFav.guardarNoticia(this.news[i]);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
  async openBrowser(i) {
    const { Browser } = Plugins;
    // On iOS, for example, this will open the URL in Safari instead of
    // the SFSafariViewController (in-app browser)
    await Browser.open({ url: this.dataFav.noticias[i].url });
  }

}
