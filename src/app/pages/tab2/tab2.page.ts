import { Component } from '@angular/core';
import { Article } from 'src/app/interface/interface';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  news: Article[];
  categories:any;
  anterior:string;
  constructor(private data: DataService) {this.categories=['business','entertainment','general','health','science','sports','technology'];}

  segmentChanged(event){
    this.anterior=event.detail.value;
    if(event.detail.value=="business"){
      this.data.getNoticiasBusiness().subscribe((resp) => {
          this.news = resp.articles;
      });
    }

    if(event.detail.value=="entertainment"){
      this.data.getNoticiasEntertainment().subscribe((resp) => {
          this.news = resp.articles;
      });
    }

    if(event.detail.value=="general"){
      this.data.getNoticiasGeneral().subscribe((resp) => {
          this.news = resp.articles;
      });
    }

    if(event.detail.value=="health"){
      this.data.getNoticiasHealth().subscribe((resp) => {
          this.news = resp.articles;
      });
    }

    if(event.detail.value=="science"){
      this.data.getNoticiasScience().subscribe((resp) => {
          this.news = resp.articles;
      });
    }

    if(event.detail.value=="sports"){
      this.data.getNoticiasSports().subscribe((resp) => {
          this.news = resp.articles;
      });
    }

    if(event.detail.value=="technology"){
      this.data.getNoticiasTechnology().subscribe((resp) => {
          this.news = resp.articles;
      });
    }

  }
  loadData(event){
    this.anteriorInfinite(event);
  }
  anteriorInfinite(event){
    if(this.anterior=="business"){
      this.data.getNoticiasBusiness().subscribe((resp) => {
        if (this.news != null) {
          this.news.push(...resp.articles);
        }
        if (this.news == null) {
          this.news = resp.articles;
        }if (event) {
          event.target.complete();
        }
        
      });
    }

    if(this.anterior=="entertainment"){
      this.data.getNoticiasEntertainment().subscribe((resp) => {
        if (this.news != null) {
          this.news.push(...resp.articles);
        }
        if (this.news == null) {
          this.news = resp.articles;
        }if (event) {
          event.target.complete();
        }
      
      });
    }

    if(this.anterior=="general"){
      this.data.getNoticiasGeneral().subscribe((resp) => {
        if (this.news != null) {
          this.news.push(...resp.articles);
        }
        if (this.news == null) {
          this.news = resp.articles;
        }if (event) {
          event.target.complete();
        }
       
      });
    }

    if(this.anterior=="health"){
      this.data.getNoticiasHealth().subscribe((resp) => {
        if (this.news != null) {
          this.news.push(...resp.articles);
        }
        if (this.news == null) {
          this.news = resp.articles;
        }if (event) {
          event.target.complete();
        }
        
      });
    }

    if(this.anterior=="science"){
      this.data.getNoticiasScience().subscribe((resp) => {
        if (this.news != null) {
          this.news.push(...resp.articles);
        }
        if (this.news == null) {
          this.news = resp.articles;
        }if (event) {
          event.target.complete();
        }
       
      });
    }
    if(this.anterior=="sports"){
      this.data.getNoticiasGeneral().subscribe((resp) => {
        if (this.news != null) {
          this.news.push(...resp.articles);
        }
        if (this.news == null) {
          this.news = resp.articles;
        }if (event) {
          event.target.complete();
        }
       
      });
    }
    if(this.anterior=="technology"){
      this.data.getNoticiasGeneral().subscribe((resp) => {
        if (this.news != null) {
          this.news.push(...resp.articles);
        }
        if (this.news == null) {
          this.news = resp.articles;
        }if (event) {
          event.target.complete();
        }
       
      });
    }

    
  }
}
