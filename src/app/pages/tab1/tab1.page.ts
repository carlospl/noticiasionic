import { Component } from '@angular/core';
import { Article } from 'src/app/interface/interface';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {
  news: Article[];
  
  constructor(private data: DataService) {}

  ngOnInit(): void {
    this.loadNews();
  }
  loadNews(event?) {
    this.data.getNoticias().subscribe((resp) => {
      console.log('noticias',resp);
      if (this.news != null) {
        this.news.push(...resp.articles);
      }
      if (this.news == null) {
        this.news = resp.articles;
      }
      if (event) {
        event.target.complete();
      }
    });
  }
  loadData(event){
    this.loadNews(event);
  }
}
