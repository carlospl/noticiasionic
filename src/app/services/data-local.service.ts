import { Injectable } from '@angular/core';
import {Storage} from '@ionic/storage-angular'
import { Article } from '../interface/interface';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {
noticias:Article[]=[];
  constructor(public storage:Storage) { 
    this.storage.create();
    this.cargarFavoritos();
  }
  guardarNoticia(noticia:Article){
    this.noticias.unshift(noticia);
    this.storage.set('favoritos',this.noticias);
  }
  async cargarFavoritos(){
    const favoritos=await this.storage.get('favoritos');
    if(favoritos){
      this.noticias=favoritos;
    }
  }
}
