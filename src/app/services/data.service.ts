import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RootObject } from '../interface/interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {


  constructor(private http: HttpClient) {}

  getNoticias(){
    return this.http.get<RootObject>(`https://newsapi.org/v2/top-headlines?language=es&apiKey=8c38bda1e0294251bb0678d9003f04f2`);
  }
  getNoticiasBusiness(){
    return this.http.get<RootObject>(`https://newsapi.org/v2/top-headlines?language=es&apiKey=8c38bda1e0294251bb0678d9003f04f2&category=business`);
  }
  getNoticiasEntertainment(){
    return this.http.get<RootObject>(`https://newsapi.org/v2/top-headlines?language=es&apiKey=8c38bda1e0294251bb0678d9003f04f2&category=entertainment`);
  }
  getNoticiasGeneral(){
    return this.http.get<RootObject>(`https://newsapi.org/v2/top-headlines?language=es&apiKey=8c38bda1e0294251bb0678d9003f04f2&category=general`);
  }
  getNoticiasHealth(){
    return this.http.get<RootObject>(`https://newsapi.org/v2/top-headlines?language=es&apiKey=8c38bda1e0294251bb0678d9003f04f2&category=health`);
  }
  getNoticiasScience(){
    return this.http.get<RootObject>(`https://newsapi.org/v2/top-headlines?language=es&apiKey=8c38bda1e0294251bb0678d9003f04f2&category=science`);
  }
  getNoticiasSports(){
    return this.http.get<RootObject>(`https://newsapi.org/v2/top-headlines?language=es&apiKey=8c38bda1e0294251bb0678d9003f04f2&category=sports`);
  }
  getNoticiasTechnology(){
    return this.http.get<RootObject>(`https://newsapi.org/v2/top-headlines?language=es&apiKey=8c38bda1e0294251bb0678d9003f04f2&category=technology`);
  }
}
// ?articles=Array(${this.variable})